package com.uuu.simpleapp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SimpleAppApplication

fun main(args: Array<String>) {
	runApplication<SimpleAppApplication>(*args)
}
